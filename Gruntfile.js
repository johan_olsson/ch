module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';'
			},
			js: {
				src: [
					'app/**/*.js'
				],
				dest: 'dist/<%= pkg.name %>.js'
			}
		},
		copy: {
			main: {
				files: [
					{src: [
						'bower_components/angular-route/angular-route.min.js',
						'bower_components/angular/angular.min.js',
						'bower_components/isotope/dist/isotope.pkgd.min.js',
						'bower_components/bootstrap/dist/css/bootstrap.min.css'
					], dest: 'dist/', filter: 'isFile', flatten: true, expand: true}

				]
			}
		},
		uglify: {
			dist: {
				files: {
					'dist/<%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
				}
			}
		},
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'dist/gameListView.html': 'app/templates/gameListView.html',
					'dist/gameView.html': 'app/templates/gameView.html',
					'dist/index.html': 'app/index.html'
				}
			}
		},
		less: {
			dist: {
				options: {
					cleancss: true
				},
				files: {
					"dist/app.css": "app/**/*.css"
				}
    			}
		},
		qunit: {
			files: ['test/**/*.html']
		},
		clean: {
			js: ["dist/**/*.js"],
			css: ["dist/**/*.css"],
			html: ["dist/**/*.html"]
		},
		watch: {
			js: {
				files: ['app/**/*.js'],
				tasks: ['clean:js', 'concat:js', 'uglify', 'copy']
			},
			html: {
				files: ['app/**/*.html'],
				tasks: ['clean:html', 'htmlmin']
			},
			css: {
				files: ['app/**/*.css'],
				tasks: ['clean:css', 'less', 'copy']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-qunit');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');

	//grunt.registerTask('test', ['qunit']);
	grunt.registerTask('default', [/*'qunit',*/ 'clean', 'copy', 'less', 'concat', 'uglify', 'htmlmin', 'watch']);

};
