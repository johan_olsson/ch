

// app.js

var app = angular.module('gameLobbyApp', ['ngRoute'])

app.config(['$routeProvider', function($routeProvider) {
   	$routeProvider
   		.when('/list', {templateUrl: 'gameListView.html', controller: 'GameListController'})
       	.when('/game/:slug', {templateUrl: 'gameView.html', controller: 'GameController'})
       	.otherwise({redirectTo: '/list'});
}]);


// clearing the cashe to keep my sanity.
app.run(function($templateCache) {
	$templateCache.removeAll();
});


// directives.js

angular.module('gameLobbyApp').directive('preLoad', [
	function() {

	return {
		restrict: 'A',
		link: function(scope, element, attrs) {

			angular.element('<img>')
				.attr('src', attrs.preLoad)
				.on('load', function() {
					element.css({
						'background-image': 'url('+attrs.preLoad+')'
					})
					.addClass('visible')
			})
		}
	}
}]);




// services.js

app.factory('getData', ['$q', '$http', '$filter', function getDataFactory($q, $http, $filter) {

	var url = "http://54.195.123.108/game-categories";

	var deferred = $q.defer()

	if (localStorage.getItem(url))
		deferred.resolve(angular.fromJson(localStorage.getItem(url)));

	else {

		$http({method: 'GET',  url: url})
			.success(function(data) {

				deferred.resolve(data._embedded.game_categories);
				localStorage.setItem(url, $filter('json')(data._embedded.game_categories));

			})

	}

	return deferred.promise;

}]);

app.service('organizeGames', [function() {

	/**
	 * @param $scope (categories must already be loaded)
	 * Maybe it's a wierd service. I'll change it if I remember to.
	**/
	return function($scope) {

		$scope.categories.forEach(function(category) {
	
			category._embedded.games.forEach(function(game) {
			
				if (!$scope.games.hasOwnProperty(game.slug)) {
		
					$scope.games[game.slug] = game;
					$scope.games[game.slug].categories = [];
				}
				
				$scope.games[game.slug].categories.push(category.slug);
		
			});
			
		})
	}

}])




// it felt silly to devide the diefferent parts
// into different files at the moment but as you
// see they are built to be able to work in that
// way too.


// controllers/GameListControllers.js

app.controller('GameListController', [
	'getData',
	'organizeGames',
	'$scope',
	'$timeout',
	function GameListController(getData, organizeGames, $scope, $timeout) {


	var iso; // Just keeping to the scope.

	$scope.categories = [];
	$scope.games = {};
	$scope.selectedCategory = '';

	$scope.search = function(event) {

		if (event.keyCode !== 13) return;

		var regex = new RegExp(event.target.value, 'i')

		iso.arrange({
			filter: function(element) {
				return element.getAttribute('name').match(regex);
			}
		});

		$scope.selectedCategory = '';
	}

	$scope.filter = function(object) {
		
		$scope.selectedCategory = object.category.slug;
		iso.arrange({ filter: '.' + object.category.slug });
	}
	
	getData.then(function(data) {

		$scope.categories = data;

		organizeGames($scope)

		/**
		 * For some reason angular need some more time here
		**/
		$timeout(function() {

			iso = new Isotope('.isotope-container', {
				itemSelector: '.isotope-item',
				layoutMode: 'fitRows'
			});

		}, 100)
	})


}])






app.controller('GameController', [
	'$routeParams',
	'$scope',
	function($routeParams, $scope) {

	$scope.slug = $routeParams.slug

}])

app.controller('MainController', [function() {

}])
